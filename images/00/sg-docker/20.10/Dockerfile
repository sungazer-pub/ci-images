FROM docker:20.10.23 as builder

# Deps
RUN apk add --update --no-cache curl git build-base openssl openssl-dev

RUN cd /tmp && git clone https://github.com/AGWA/git-crypt.git && cd git-crypt && CXXFLAGS='-DOPENSSL_API_COMPAT=0x30000000L' make -j4 && make install && rm -fr /tmp/git-crypt

ARG TARGETOS=linux
ARG TARGETARCH=amd64
ARG KUBECTL_VERSION=1.23.0
# https://github.com/kubernetes-sigs/kustomize/releases
ARG KUSTOMIZE_VERSION=5.0.0
# https://github.com/GoogleContainerTools/skaffold/releases
ARG SKAFFOLD_VERSION=2.1.0

# swarm-cli
ADD https://github.com/sungazer-io/swarm-cli/releases/download/latest/swarm-cli.alpine /usr/local/bin/swarm-cli
RUN chmod +x /usr/local/bin/swarm-cli && /usr/local/bin/swarm-cli

# Kubectl & Kustomize
RUN set -ex; \
    curl -fL https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl -o kubectl && \
    chmod +x kubectl && mv kubectl /usr/local/bin/ && kubectl version --short=true --client=true

RUN set -ex; \
    curl -fL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz | tar xz && \
    chmod +x kustomize && mv kustomize /usr/local/bin/ && kustomize version

# Skaffold
RUN set -ex; \
    curl -fL https://storage.googleapis.com/skaffold/releases/v${SKAFFOLD_VERSION}/skaffold-linux-amd64 -o skaffold && \
    chmod +x skaffold && mv skaffold /usr/local/bin/ && skaffold version

# docker-compose
# RUN apk add --update --no-cache py3-pip python3-dev libffi-dev openssl-dev gcc libc-dev rust cargo make zlib-dev
# RUN export MAKEFLAGS="-j$(nproc)"
# RUN pip install pyinstaller
# RUN pip install docker-compose==1.28.6
# RUN pyinstaller --onefile /usr/bin/docker-compose

#####
# Final image
#####

# Looks like 20.10.8 is broken (import errors on docker-compose), probably better to stick to this version
FROM docker:20.10.23

RUN apk add --update --no-cache \
    openssl \
    libstdc++ \
    curl \
    git \
    gcompat \
    docker-compose

COPY --from=builder /usr/local/bin/git-crypt            /usr/local/bin/git-crypt
COPY --from=builder /usr/local/bin/swarm-cli            /usr/local/bin/
COPY --from=builder /usr/local/bin/kubectl              /usr/local/bin/kubectl
COPY --from=builder /usr/local/bin/kustomize            /usr/local/bin/kustomize
COPY --from=builder /usr/local/bin/skaffold             /usr/local/bin/skaffold
# COPY --from=builder /dist/docker-compose                /usr/local/bin/docker-compose

# Test runs for commands
RUN set -ex; \
    mkdir /tmp/test && cd /tmp/test && git init && git crypt init && rm -rf /tmp/test && cd / && \
    swarm-cli && \
    kubectl version --short=true --client=true && \
    kustomize version && \
    skaffold version && \
    docker-compose --version 

COPY ./res/bin /usr/local/bin/

RUN chmod +x /usr/local/bin/*